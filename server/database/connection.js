import mongoose from 'mongoose'

const connectionString = process.env.MONGODB_URI || process.env.DEV_MONGODB_URI

mongoose.connect(connectionString, {
  useUnifiedTopology: true,
  connectTimeoutMS: 1000,
  useNewUrlParser: true,
  useCreateIndex: true
})

const db = mongoose.connection

db.on('error', err => process.stdout.write('Connection to MongoDB database failed!\n', err.message))
db.on('connected', () => process.stdout.write('MongoDB database connected!\n'))
db.on('disconnected', () => process.stdout.write('Database disconnected!\n'))
