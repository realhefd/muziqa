import { handleErrors, parseAlbum, createNewAlbum } from '../../utils/helpers'
import { lastfm as LastFm } from '../../utils/last.fm'
import axios from 'axios'

const LAST_FM_API_KEY = process.env.VUE_APP_LAST_FM_API_KEY
const API_PATH = `${process.env.VUE_APP_API_PATH}albums`

const state = {
  albums: [],
  albumOperations: [
    {
      color: 'error',
      icon: 'mdi-delete-outline',
      handler: 'removeAlbum',
      text: 'Remove Album',
      origin: 'shelf'
    },
    {
      color: 'success',
      icon: 'mdi-plus',
      handler: 'addAlbum',
      text: 'Add Album',
      origin: 'search'
    }
  ]
}

const getters = {
  albumOperations: state => state.albumOperations,
  albums: state => state.albums
}

const actions = {
  async fetchAlbums (context, payload) {
    context.dispatch('updateSkeletonLoader', { album: !!payload.loader })
    context.commit('RESET_ALBUMS', [])

    await axios.get(API_PATH).then(response => {
      response.data.forEach(album => {
        if (album.manual) createNewAlbum(context, parseAlbum(album), false)
        else {
          const params = {
            mbid: album.mbid,
            album: album.name,
            artist: album.artist.name
          }

          LastFm(LAST_FM_API_KEY).albumInfo(params, (error, albumInfo) => {
            if (error) return handleErrors(context, error, 'internet')
            createNewAlbum(context, Object.assign(albumInfo, album), false)
          })
        }
      })

      setTimeout(() => context.dispatch('updateSkeletonLoader', { album: false }), 1000)
    }).catch(error => handleErrors(context, error, 'database'))
  },

  async addAlbum (context, payload) {
    await axios.post(API_PATH, payload).then(response => {
      const album = response.data

      if (album.exists) {
        return context.dispatch('showSnackbar', {
          text: 'This album already exists in your shelf',
          color: 'info'
        })
      }

      if (album.manual) createNewAlbum(context, parseAlbum(album), true)
      else {
        const params = {
          mbid: album.mbid,
          album: album.name,
          artist: album.artist.name
        }
        LastFm(LAST_FM_API_KEY).albumInfo(params, (error, albumInfo) => {
          error
            ? handleErrors(context, error, 'internet')
            : createNewAlbum(context, albumInfo, true)
        })
      }
    }).catch(error => handleErrors(context, error, 'database'))
  },

  async removeAlbum (context, payload) {
    await axios.delete(`${API_PATH}/${payload.albumId}`)
      .then(response => {
        context.commit('DELETE_ALBUM', {
          albumId: payload.albumId,
          searchResults: context.getters.searchResults
        })

        return context.dispatch('showSnackbar', {
          text: response.data.removed
            ? 'This album has already been deleted or does not exist!'
            : 'Album successfully removed from shelf',
          color: 'error'
        })
      })
      .catch(error => handleErrors(context, error, 'database'))
  }
}

const mutations = {
  CREATE_ALBUM: (state, album) => state.albums.unshift(album),
  RESET_ALBUMS: (state, data) => (state.albums = data),
  DELETE_ALBUM: (state, data) => {
    data.searchResults.splice(data.searchResults.findIndex(o => o.id === data.albumId), 1)
    state.albums.splice(state.albums.findIndex(o => o.id === data.albumId), 1)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
