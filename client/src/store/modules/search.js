import { lastfm as LastFm } from '../../utils/last.fm'
import { handleErrors } from '../../utils/helpers'

const LAST_FM_API_KEY = process.env.VUE_APP_LAST_FM_API_KEY

const state = {
  searchFilters: [
    { model: false, label: 'Include Artists' },
    { model: true, label: 'Include Albums' }
  ],
  searchBarVisibility: true,
  searchResults: [],
  searchQuery: '',
  searchMode: {
    state: false,
    origin: null
  }
}

const getters = {
  searchBarVisibility: state => state.searchBarVisibility,
  searchFilters: state => state.searchFilters,
  searchResults: state => state.searchResults,
  searchQuery: state => state.searchQuery,
  searchMode: state => state.searchMode
}

const actions = {
  async searchLastFmAlbums (context, key) {
    const params = {
      album: key,
      limit: 30
    }
    LastFm(LAST_FM_API_KEY).albumSearch(params, async (error, searchResults) => {
      if (error) return handleErrors(context, error, 'internet')
      await context.commit('SET_SEARCH_RESULTS', searchResults)
      context.dispatch('updateSkeletonLoader', { album: false })
    })
  },

  async searchShelfAlbums ({ getters, commit }, key) {
    const includeArtist = getters.searchFilters[0].model
    const includeAlbum = getters.searchFilters[1].model

    const results = await getters.albums.filter(album => {
      const artistName = album.artist.name.toLowerCase()
      const albumName = album.name.toLowerCase()

      return (includeAlbum && includeArtist)
        ? albumName.includes(key) || artistName.includes(key)
        : includeAlbum
          ? albumName.includes(key)
          : includeArtist
            ? artistName.includes(key)
            : Error('Please enter a search term!')
    })

    commit('SET_SEARCH_RESULTS', results)
  },

  async searchShelfArtists ({ getters, commit }, key) {
    const results = await getters.artists
      .filter(artist => artist.name.toLowerCase().includes(key))

    commit('SET_SEARCH_RESULTS', results)
  },

  searchData ({ dispatch, getters }, payload) {
    dispatch('updateSearchMode', { state: true, origin: payload.origin })
    dispatch('updateConnectivityProbe', { internet: true, database: true })
    dispatch('clearSearchResults')
    dispatch('updateSkeletonLoader', payload.origin.includes('Artist')
      ? { artist: true }
      : { album: true })

    if (payload.origin === 'AddLastFmAlbum') {
      dispatch('searchLastFmAlbums', getters.searchQuery.toLowerCase())
    } else if (payload.origin === 'Home') {
      dispatch('searchShelfAlbums', getters.searchQuery.toLowerCase())
        .then(() => dispatch('updateSkeletonLoader', { album: false }))
    } else if (payload.origin === 'Artists') {
      dispatch('searchShelfArtists', getters.searchQuery.toLowerCase())
        .then(() => dispatch('updateSkeletonLoader', { artist: false }))
    } else {
      dispatch('updateSkeletonLoader', { artist: false, album: false })
      return Error('Please enter a search term!')
    }
  },

  updateSearchBarVisibility: (context, payload) => context.commit('SET_SEARCH_BAR_VISIBILITY', payload),
  updateSearchQuery: (context, payload) => context.commit('SET_SEARCH_QUERY', payload.key),
  updateSearchMode: (context, payload) => context.commit('UPDATE_SEARCH_MODE', payload),
  clearSearchResults: (context) => context.commit('CLEAR_SEARCH_RESULTS')
}

const mutations = {
  SET_SEARCH_BAR_VISIBILITY: (state, bool) => (state.searchBarVisibility = bool),
  UPDATE_SEARCH_MODE: (state, data) => Object.assign(state.searchMode, data),
  SET_SEARCH_RESULTS: (state, data) => (state.searchResults = data),
  SET_SEARCH_QUERY: (state, query) => (state.searchQuery = query),
  CLEAR_SEARCH_RESULTS: (state) => (state.searchResults = [])
}

export default {
  state,
  getters,
  actions,
  mutations
}
