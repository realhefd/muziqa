import capitalize from 'node-capitalize'
import Artist from '../models/artist.js'
import Album from '../models/album.js'
import { v4 as uuidv4 } from 'uuid'
import mongoose from "mongoose"
import Joi from 'joi'

const create = async artist => {
  const schema = Joi.object().keys({
    mbid: Joi.required(),
    name: Joi.string().max(500).required(),
    manual: Joi.boolean().required().strict()
  })

  const { error } = schema.validate(artist)

  if (error == null) {
    Object.assign(artist, {
      mbid: artist.mbid || uuidv4()
    })

    const artistExists = await Artist.findOne({ name: artist.name })

    return artistExists
      ? parseArtists(Object.assign(artistExists, { exists: true }))[0]
      : parseArtists(await new Artist(artist).save())[0]
  } return process.stdout.write(error)
}

const getAll = async () => parseArtists(await Artist.find())

const getByAlbumID = async id => {
  if( !mongoose.Types.ObjectId.isValid(id) ) return []

  const album = await Album.findById(id)

  return album
    ? parseArtists(await Artist.findById(album.artistId))
    : []
}

const getByAlbumName = async filter => {
  const album = await Album.findOne(filter)
  
  return album
    ? parseArtists(await Artist.findById(album.artistId))
    : [] 
}

const getByID = async id => {
  if( !mongoose.Types.ObjectId.isValid(id) ) return []

  const artist = await Artist.findById(id)

  return artist ? parseArtists(artist) : []
}

const getByName = async filter => parseArtists(await Artist.find(filter))

const remove = async filter => parseArtists(await Artist.findByIdAndDelete(filter))

const update = async (filter, update) => {
  const updatedArtist = await Artist.findOneAndUpdate(filter, update, { new: true })
  return parseArtists(updatedArtist)
}

const parseArtists = (artists) => {
  artists = artists instanceof Array ? artists : [artists]

  return artists.map(artist => {
    return {
      id: artist.id,
      mbid: artist.mbid,
      name: artist.manual ? capitalize(artist.name, true) : artist.name,
      manual: artist.manual,
      exists: artist.exists || false
    }
  }).sort()
}

export default {
  getByAlbumName,
  getByAlbumID,
  getByName,
  getByID,
  getAll,
  create,
  update,
  remove
}
