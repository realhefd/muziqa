
import mockdata from '../../mockdata.js'
import color from 'colors-cli/safe.js'
import axios from 'axios'

process.stdout.write(
  `${color.x201('Initializing Initial Database State With MockData')}\n
    Creating albums and artists.....
  `)

const { artists, albums } = mockdata

albums.map(async (album, i) => {
  const params = {
    manual: false,
    mbid: album.id,
    name: album.name,
    artistMbid: artists.find(artist => artist.id === album.artistId).id,
    artistName: artists.find(artist => artist.id === album.artistId).name
  }

  const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))
  await sleep(100 * i)

  const createAlbum = payload => axios.post(process.env.API_PATH, payload)
    .then(response => response)

  createAlbum(params).then(result => {
    const { exists, name } = result.data

    const message = `
        ${color.green('Completed')}
        Status:${color.x45(result.status)}
        ${exists
          ? color.yellow('Album already exists in DB!')
          : 'Created Album: ' + name}\n`
    process.stdout.write(message)
  })
})
