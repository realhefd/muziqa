import Vue from 'vue'
import Vuex from 'vuex'
import mockAlbums from '../../../tests/unit/mockData/albums.json'

import jest from 'jest-mock'

const mock = jest.fn()

Vue.use(Vuex)

export const state = {
  albums: mockAlbums,
  confirmModal: {
    identifier: null,
    operation: null,
    state: false,
    title: null,
    info: null
  }
}

export const getters = {
  albums: mock.mockReturnValue(state.albums),
  confirmModal: mock.mockReturnValue({
    identifier: state.confirmModal.identifier,
    operation: state.confirmModal.operation,
    state: state.confirmModal.state,
    title: state.confirmModal.title,
    info: state.confirmModal.info
  })
}

export const mutations = {
  SET_CONFIRM_MODAL: mock,
  CREATE_ALBUM: mock,
  RESET_ALBUMS: mock,
  DELETE_ALBUM: mock
}

export const actions = {
  updateConfirmModal: mutations.SET_CONFIRM_MODAL,
  removeAlbum: mutations.DELETE_ALBUM,
  addAlbum: mutations.CREATE_ALBUM,
  fetchAlbums: mock
}

export function __createMocks (custom = { getters: {}, mutations: {}, actions: {}, state: {} }) {
  const mockGetters = Object.assign({}, getters, custom.getters)
  const mockMutations = Object.assign({}, mutations, custom.mutations)
  const mockActions = Object.assign({}, actions, custom.actions)
  const mockState = Object.assign({}, state, custom.state)

  return {
    getters: mockGetters,
    mutations: mockMutations,
    actions: mockActions,
    state: mockState,
    store: new Vuex.Store({
      getters: mockGetters,
      mutations: mockMutations,
      actions: mockActions,
      state: mockState
    })
  }
}

export const store = __createMocks().store
