
import capitalize from 'node-capitalize'
import Artist from '../models/artist.js'
import Album from '../models/album.js'
import { v4 as uuidv4 } from 'uuid'
import mongoose from "mongoose"
import Joi from 'joi'

const create = async album => {
  const schema = Joi.object().keys({
    mbid: Joi.required(),
    name: Joi.string().required(),
    artistId: Joi.required(),
    manual: Joi.boolean().required().strict()
  })

  const { error } = schema.validate(album)

  if (error == null) {
    Object.assign(album, {
      mbid: album.mbid || uuidv4(),
      artistId: album.artistId
    })

    const albumExists = await Album.findOne({ name: album.name })

    return albumExists
      ? parseAlbums(Object.assign(albumExists, { exists: true}), await Artist.find())[0]
      : parseAlbums(await new Album(album).save(), await Artist.find())[0]
  } return process.stdout.write(error)
}

const getAll = async () => parseAlbums(await Album.find(), await Artist.find())

const getByArtistID = async filter => parseAlbums(await Album.find(filter), await Artist.find())

const getByArtistName = async filter => {
  const artist = await Artist.findOne(filter)
  if (!artist) return []

  const albums = await Album.find({ artistId: artist.id })

  return parseAlbums(albums, await Artist.find())
}

const getByID = async id => {
  if( !mongoose.Types.ObjectId.isValid(id) ) return []

  const album = await Album.findById(id)

  return album
    ? parseAlbums(album, await Artist.find())
    : []
}

const getByName = async filter => parseAlbums(await Album.find(filter), await Artist.find())

const remove = async filter => {
  const albumExists = await Album.find(filter)

  return albumExists.length
    ? parseAlbums(await Album.findByIdAndDelete(filter), await Artist.find())[0]
    : { removed: true }
}

const update = async (filter, update) => {
  const updatedAlbum = await Album.findOneAndUpdate(filter, update, { new: true })
  return parseAlbums(updatedAlbum, await Artist.find())
}

const parseAlbums = (albums, artists) => {
  albums = albums instanceof Array ? albums : [albums]

  return albums.map(album => {
    return {
      id: album.id,
      mbid: album.mbid,
      name: album.manual ? capitalize(album.name, true) : album.name,
      manual: album.manual,
      exists: album.exists || false,
      artist: [artists.find(artist => artist.id === album.artistId)]
        .map(artist => {
          return {
            id: artist.id,
            mbid: artist.mbid,
            name: artist.manual ? capitalize(artist.name, true) : artist.name,
            manual: artist.manual
          }
        })[0]
    }
  }).sort()
}

export default {
  getByArtistName,
  getByArtistID,
  getByName,
  getByID,
  getAll,
  create,
  update,
  remove
}
