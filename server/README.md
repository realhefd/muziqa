# Project description: Backend

This is the backend part of the application: Muziqa

> This Application runs on an ExpressJs Backend with Mongoose and MongoDB for permanent storage and RESTful APIs

<br/>
Please use the following instructions to setup the project and run the application
<br/>

## Setting up the Application
<br/>

### Setting up MongoDB:

**[MongoDB Installation on Windows](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/)**

**[MongoDB Installation on macOS](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/)**

**[MongoDB Installation on Linux](https://docs.mongodb.com/manual/administration/install-on-linux/)**
<br/><br/>

### Setting up the Backend App :

#### *First ensure that ``MongoDB`` has been installed successfully and that the ``mongod`` service is running on your system*

```
$ cd server
$ npm install
$ npm run start
```
### Initializing the Database:
##### Open a different terminal window and run the following command to initialize the database with the mockdata to set up the initial state of the application
```
$ cd server
$ npm run init-database
```
-
-
-

# Assignment description: Exercise #3
Here you are going to make Node.js back-end application and design API for it. It should provide all of the facilities that front-end application needs without any other front-end API calls. For the Node.js backend you can use libraries like Express.js.

You do not request to implement permanent storage. But if you wish to do so, then getting it running should be simple and instructions clear.

We expect that the project can be run by using a single command after dependencies have been installed. For example:

```
$ npm install
$ npm run demo-backend
```

For implementing the API you have two choices. You can choose between `GraphQL` and `REST`.

### API functionality
As we also ask you to design a simple API we only give few general guidelines on what the API should do. Please also see the data structures for the mock data.

#### Requesting data
* Get all albums in the virtual record shelf
* Proper filtering of the data where needed. For example get all albums by `artist name` or `artist id`

#### Inserting / updating data
* Add an album
* Remove an album
* Add an artist
