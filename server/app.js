import bodyParser from 'body-parser'
import express from 'express'
import morgan from 'morgan'
import cors from 'cors'
import routes from './routes/index.js'

// Database
import './database/connection.js'

// Express App
const app = express()

// Middleware
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(morgan('tiny'))
app.use(cors())

// Routes
app.get('/', (req, res) => res.send('Go to /api'))
app.use('/api', routes)

// Server
const port = process.env.PORT || 4000
app.listen(port, () => process.stdout.write(`Listening on Port:${port}...\n`))
