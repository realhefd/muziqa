module.exports = {
  publicPath: '/',
  chainWebpack: config => {
    config.plugin('html').tap(args => {
      args[0].title = 'Muziqa'
      return args
    })
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: '@import "~@/scss/variables.scss";'
      }
    }
  }
}
