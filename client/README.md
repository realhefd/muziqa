# Project description: Frontend

This is the frontend part of the application: Muziqa

> This application is built with VueJs and Vuex for state management with VuetifyJs as the Material Design UI framework.

Please use the following instructions to setup the project and run the application

## Setting up the Application
<br/>

### Frontend App

```
$ cd client
$ npm install
$ npm run client
```

### Unit Testing

```
$ cd client
$ npm run test:unit
```

### Unit Testing: Watch

```
$ cd client
$ npm run test:unit:watch
```

### Running Linter

```
$ cd client
$ npm run lint
```

### Building for Production

```
$ cd client
$ npm run build
```
-
-
-
# Assignment description
The task is to make a virtual record shelf where you can add artists and albums. The application should work on all modern desktop and mobile browsers.

We also integrate the record shelf to the Last.fm API to get more info to display the albums. From the data Last.fm API provides, we will only use a picture, tracklisting and track lengths for the albums. For Last.fm API, you need to make an account and create API authorization key. Please note that the key is for your personal Last.fm account.

https://www.last.fm/api

There are few libraries for the Last.fm you can use. Of course, you can use any HTTP client like Axios and implement the API calls you need.

If you want to do #2 without doing it in parallel with #3 you can use, for example, this library https://github.com/fxb/javascript-last.fm-api

For Node.js environment there is available https://www.npmjs.com/package/last-fm


## Mockdata for the exercises
The project's mock data is in [mockdata.js](mockdata.js) file in the project root directory. A basic array is used to store a set of objects of these two types. UUID V4 is to be used as universally unique identifiers.

### Artist
```
{
  id: "984ee0e0-1f01-4855-853a-ebc3a372653d", 
  name: "John Frusciante"
}
```

### Album
```
{
  id: "69fcea1a-d21b-4734-baa6-0285c44875fb",
  artistId: "984ee0e0-1f01-4855-853a-ebc3a372653d",
  name: "Niandra LaDes and Usually Just a T-Shirt"
}
```

## Exercise #2
Here you are going to make the front-end part for displaying and adding data to the virtual record shelf. For the initial state, you will use the mock data provided in this repository. You are not required to save the state permanently.

You can do this exercise separately or together with exercise #3 and integrating it into the backend on the fly.

For the front-end part, we ask you to choose between two front-end stacks, `React` or `Vue.js`. For the state keeping, we recommend that you use either `React-Redux` or new `contex-API` for React and `Vuex` for Vue.js.

If you do not want to use state management, it is fine too. What we expect are good coding practices and SoC/SRP principles when appropriate.

We expect that the project can be run by just using a single command after dependencies are installed. For example:

```
$ npm install
$ npm run demo-ui
```

### UI and features
<img src="img/main_page.png" alt="Main page functionality" width="80%"/>

Here is a suggestion for the main page layout of the virtual record shelf on desktop browser. There are only a few functionalities:

* Search box to filter results
* Checkboxes to select where search filter applies. When both are set, and the search term matches the artist or album name, the match is displayed. If only the artist is selected, then only albums by artist matching are displayed.
* A button to add a new album. It is up to you how you manage adding albums and artists to the database. Do not add the same artist or album with the exact same name multiple times.
* Area where the albums added by the user are being displayed. The initial state for the record collection is to show the mock data. You get the album cover (size 300x300px), tracklisting and track lengths from the Last.fm API.
* ... menu will display a drop down menu with an option to remove album. You can use this to remove the album from you virtual record collection.

_**Please note that the track listing can be missing for some albums!**_

## General guidelines for the project
1. After cloning, start to work in your own repository. So import this repository into your private repository in GitLab (or GitHub). Add repository access to the following user accounts: `vkoivula` and `aepgit`. Please do not fork this project.

1. If you take code examples from Stack Overflow or other places. Please add the URL into the comments.  Otherwise, we do not appreciate it.

1. If you are unable to complete exercises fully, commit the code you have done. Comment out or otherwise disable parts that are not working and are causing your code to crash.

1. We should be able to run the exercise with just one command. The procedure is described more closely in each exercise.
