import mongoose from 'mongoose'

const AlbumSchema = new mongoose.Schema({
  mbid: {
    type: String,
    required: true,
    unique: true
  },
  name: {
    type: String,
    required: true
  },
  artistId: {
    type: String,
    required: true
  },
  manual: {
    type: Boolean,
    required: true
  },
  created: {
    type: Date,
    default: Date.now
  }
})

export default mongoose.model('Album', AlbumSchema)
