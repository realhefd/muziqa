import express from 'express'
import artists from '../controller/artists.js'
const router = express.Router()

// Filter and Get Artist(s)
router.get('/', async (req, res) => {
  const { album, albumId, name } = req.query

  const results = albumId
    ? await artists.getByAlbumID(albumId)
    : album
      ? await artists.getByAlbumName({ name: album })
      : name
        ? await artists.getByName({ name: name })
        : await artists.getAll()

  res.json(results)
})

// Create New Artist
router.post('/', async (req, res) => {
  const createdArtist = await artists.create(req.body)
  if (createdArtist.exists) res.json({ message: 'Artist exists' })
  res.json(createdArtist)
})

// Get Specific Artist: By ID
router.get('/:id', async (req, res) => {
  const results = await artists.getByID(req.params.id)
  res.json(results)
})

// Delete Artist
router.delete('/:id', async (req, res) => {
  const deletedArtist = await artists.remove({ _id: req.params.id })
  res.json(deletedArtist)
})

// Update Artist
router.patch('/:id', async (req, res) => {
  const updatedArtist = await artists.update({ _id: req.params.id }, { $set: req.body })
  res.json(updatedArtist)
})

// Export default router
export default router
