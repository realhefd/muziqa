import { handleErrors, parseArtist } from '../../utils/helpers'
import { lastfm as LastFm } from '../../utils/last.fm'
import axios from 'axios'

const LAST_FM_API_KEY = process.env.VUE_APP_LAST_FM_API_KEY
const API_PATH = `${process.env.VUE_APP_API_PATH}artists`

const state = {
  artists: []
}

const getters = {
  artists: state => state.artists
}

const actions = {
  async fetchArtists (context) {
    context.dispatch('updateSkeletonLoader', { artist: true })
    context.commit('RESET_ARTISTS', [])

    await axios.get(API_PATH).then(response => {
      response.data.forEach(artist => {
        if (artist.manual) context.commit('CREATE_ARTIST', parseArtist(artist))
        else {
          const params = { artist: artist.name }
          LastFm(LAST_FM_API_KEY).artistInfo(params, (error, artistInfo) => {
            if (error) return handleErrors(context, error, 'internet')
            context.commit('CREATE_ARTIST', Object.assign(artistInfo, artist))
          })
        }
      })

      setTimeout(() => context.dispatch('updateSkeletonLoader', { artist: false }), 1000)
    }).catch(error => handleErrors(context, error, 'database'))
  }
}

const mutations = {
  CREATE_ARTIST: (state, artist) => (state.artists.unshift(artist)),
  RESET_ARTISTS: (state, data) => (state.artists = data)
}

export default {
  state,
  getters,
  actions,
  mutations
}
