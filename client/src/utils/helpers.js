const handleErrors = (context, error, type) => {
  if (error.message.includes('Network Error')) {
    context.dispatch('updateSkeletonLoader', { album: false })
    context.dispatch('updateConnectivityProbe', { [type === 'internet' ? 'internet' : 'database']: false })
    context.dispatch('showSnackbar', {
      text: `Please check your ${type === 'internet' ? 'internet' : 'database'} connection`,
      color: 'error'
    })
    return
  }
  return context.dispatch('showSnackbar', { text: error.message, color: 'error' })
}

const createNewAlbum = (context, album, showMessage) => {
  context.commit('CREATE_ALBUM', album)

  if (showMessage) {
    context.dispatch('showSnackbar', {
      text: 'Album successfully added to shelf',
      color: 'success'
    })
  }
}

const parseAlbum = album => {
  return {
    artist: {
      id: album.artist.id,
      manual: album.artist.manual,
      mbid: album.artist.mbid,
      name: album.artist.name
    },
    exists: album.exists,
    id: album.id,
    image: require('@/assets/logo.png'),
    listeners: '0',
    manual: album.manual,
    mbid: album.mbid,
    name: album.name,
    paginationLength: 0,
    paginationPage: 0,
    tracks: []
  }
}

const parseArtist = artist => {
  return {
    image: require('@/assets/logo.png'),
    name: artist.name,
    manual: artist.manual,
    id: artist.id,
    listeners: 0,
    url: null
  }
}

export {
  createNewAlbum,
  handleErrors,
  parseArtist,
  parseAlbum
}
