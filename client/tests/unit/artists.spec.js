import Vue from 'vue'
import Vuetify from 'vuetify'
import { mount, createLocalVue } from '@vue/test-utils'
import ArtistList from '@/components/Artists/ArtistList'
import artists from './mockData/artists.json'
Vue.use(Vuetify)

const localVue = createLocalVue()

const propsData = {
  artists: artists
}

describe('Muziqa Artist List Tests', () => {
  let vuetify
  let wrapper
  beforeEach(() => {
    vuetify = new Vuetify()
    wrapper = mount(ArtistList, {
      localVue,
      vuetify,
      propsData
    })
  })

  it('Should render the ArtistList component correctly', () => {
    expect(wrapper.exists()).toBeTruthy()
    expect(wrapper.find('#ArtistList').exists()).toBe(true)
  })

  it('Should check and pass if there are artists in the shelf', () => {
    const artists = wrapper.vm.artists
    expect(artists).toBeTruthy()
    expect(artists.length).toBeTruthy()
  })

  it('Should render all shelf artists on the ArtistList component correctly', () => {
    expect(wrapper.find('.artists.mx-auto').exists()).toBe(true)
  })

  it(`Should return the number of artists in shelf to be ${propsData.artists.length}`, () => {
    const artists = wrapper.vm.artists
    expect(artists.length).toBeTruthy()
    expect(artists.length).toBe(propsData.artists.length)
    expect(wrapper.findAll('.artists.mx-auto').length).toBe(propsData.artists.length)
  })
})
