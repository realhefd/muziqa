import express from 'express'
import albums from '../controller/albums.js'
import artists from '../controller/artists.js'
const router = express.Router()

// Filter and Get Album(s)
router.get('/', async (req, res) => {
  const { artist, artistId, name } = req.query

  const results = artistId
    ? await albums.getByArtistID({ artistId: artistId })
    : artist
      ? await albums.getByArtistName({ name: artist })
      : name
        ? await albums.getByName({ name: name })
        : await albums.getAll()

  res.json(results)
})

// Create New Album
router.post('/', async (req, res) => {
  const { mbid, name, manual, artistMbid, artistName } = req.body

  const artist = {
    mbid: artistMbid,
    name: artistName,
    manual: manual
  }

  const album = {
    manual: manual,
    mbid: mbid,
    name: name
  }

  const createAlbum = async (createdArtist, album) => {
    Object.assign(album, { artistId: createdArtist.id })
    const createdAlbum = await albums.create(album)

    res.json(createdAlbum.exists ? { exists: true } : createdAlbum)
  }

  const createdArtist = await artists.create(artist)
  createAlbum(createdArtist, album)
})

// Get Specific Album: By ID
router.get('/:id', async (req, res) => {
  const results = await albums.getByID(req.params.id)
  res.json(results)
})

// Delete Album
router.delete('/:id', async (req, res) => {
  const deleteArtist = async deletedAlbum => {
    await artists.remove({ _id: deletedAlbum.artist.id })
    res.json(deletedAlbum)
  }

  const deletedAlbum = await albums.remove({ _id: req.params.id })
  const remainingAlbums = deletedAlbum.removed
    ? [deletedAlbum]
    : await albums.getByArtistID({ artistId: deletedAlbum.artist.id })

  remainingAlbums.length || deletedAlbum.removed
    ? res.json(deletedAlbum)
    : deleteArtist(deletedAlbum)
})

// Update Album
router.patch('/:id', async (req, res) => {
  const updatedAlbum = await albums.update({ _id: req.params.id }, { $set: req.body })
  res.json(updatedAlbum)
})

export default router
