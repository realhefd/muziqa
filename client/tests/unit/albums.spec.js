import Vue from 'vue'
import Vuex from 'vuex'
import Vuetify from 'vuetify'
import { mount, createLocalVue } from '@vue/test-utils'
import AlbumList from '@/components/Albums/AlbumList'
import albums from './mockData/albums.json'
import { __createMocks as createStoreMocks } from '../../src/store'
Vue.use(Vuetify)
jest.mock('../../src/store')
const localVue = createLocalVue()
localVue.use(Vuex)

const propsData = {
  albums: albums,
  albumOperations: [
    {
      color: 'error',
      icon: 'mdi-delete-outline',
      handler: 'removeAlbum',
      text: 'Remove Album',
      origin: 'shelf'
    },
    {
      color: 'success',
      icon: 'mdi-plus',
      handler: 'addAlbum',
      text: 'Add Album',
      origin: 'search'
    }
  ]
}

describe('Muziqa Album List Tests', () => {
  let storeMocks
  let vuetify
  let wrapper
  beforeEach(() => {
    storeMocks = createStoreMocks()

    vuetify = new Vuetify()
    wrapper = mount(AlbumList, {
      store: storeMocks.store,
      localVue,
      vuetify,
      propsData
    })
  })

  it('Should render the AlbumList component correctly', () => {
    expect(wrapper.exists()).toBeTruthy()
    expect(wrapper.find('#AlbumList').exists()).toBe(true)
  })

  it('Should check and pass if there are albums in the shelf', () => {
    const albums = wrapper.vm.albums
    expect(albums).toBeTruthy()
    expect(albums.length).toBeTruthy()
  })

  it('Should render all shelf albums on the AlbumList component correctly', () => {
    expect(wrapper.find('.albums.mx-auto').exists()).toBe(true)
  })

  it(`Should return the number of albums in shelf to be ${propsData.albums.length}`, () => {
    const albums = wrapper.vm.albums
    expect(albums.length).toBeTruthy()
    expect(albums.length).toBe(propsData.albums.length)
    expect(wrapper.findAll('.albums.mx-auto').length).toBe(propsData.albums.length)
  })

  it('Should add an album to the shelf when [Add Album] button is clicked', async () => {
    expect(wrapper.exists()).toBeTruthy()

    const addBtn = wrapper.find('.addAlbum')
    expect(addBtn.exists()).toBe(true)
    addBtn.trigger('click')
    await Vue.nextTick()

    expect(storeMocks.actions.addAlbum).toBeCalled()
  })

  it('Should remove an album from the shelf when [Remove Album] button is clicked', async () => {
    expect(wrapper.exists()).toBeTruthy()

    const deleteBtn = wrapper.find('.removeAlbum')
    expect(deleteBtn.exists()).toBe(true)
    deleteBtn.trigger('click')
    await Vue.nextTick()

    expect(storeMocks.actions.removeAlbum).toBeCalled()
  })
})
