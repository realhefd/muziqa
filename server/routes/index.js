import Artist from './artist.js'
import Album from './album.js'

import express from 'express'

const app = express()

app.get('/', (req, res) => res.send('Muziqa APIs/Backend ready!')) // API readiness probe
app.use('/artists', Artist)
app.use('/albums', Album)

export default app
